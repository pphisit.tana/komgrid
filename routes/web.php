<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('get_data', 'SessionController@get_data');
Route::get('add_data', 'SessionController@add_data');


Route::get('index', 'SessionController@index');
Route::post('login', 'SessionController@login');
Route::get('show_user', 'SessionController@show_user');
Route::post('delete_user/{id}', 'SessionController@delete_user');
