@extends('layouts.app')

@section('content')

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
    function del_user(id){
        // alert(id);
        check = confirm('ยืนยันการลบข้อมูล');
        if(check == true){
            $('#delete_user' + id).submit();
        }
    }
</script>

<div style="margin-left: 2%;margin-right: 2%;margin-top:2%;">
    <div class="row justify-content-center">
        <div class="col-lg-2">
            <ul>
                <li style="margin: 5px 0 0 0;font-size: 18px;">หน้าหลัก</li>
            </ul>
        </div>
        <div class="col-lg-10">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ชื่อ</th>
                                <th>นามสกุล</th>
                                <th>ตำแหน่ง</th>
                                <th>เวลาที่เข้าใช้งานล่าสุด</th>
                                <th>เครื่องมือ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($users_data as $data)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $data->first_name }}</td>
                                    <td>{{ $data->last_name }}</td>
                                    <td>{{ $data->position->position }}</td>
                                    <td>{{ $data->last_login_at }}</td>
                                        
                                    <td>
                                        <a href="edit_user/{{$data->id}}">แก้ไข</a>
                                        <form method="POST" id="delete_user{{$data->id}}" action="delete_user/{{$data->id}}">
                                            {{ csrf_field() }}
                                            <a href="#" onclick="del_user({{$data->id}})">ลบ</a>
                                        </form>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>

@endsection