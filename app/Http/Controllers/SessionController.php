<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Users;
use App\Position;
use App\articles;

class SessionController extends Controller
{
    public function get_data(){
        
        // $position = Position::all();
        // dd($position);
    }

    public function add_data(){

        $arrs = [
            0 => ['joseph123', '1234eph', 'Joseph', 'Holden', '1'],
            1 => ['jasmine567', '7890jas', 'Jasmine', 'Glover', '2'],
            2 => ['barbara234', '554abc', 'Barbara', 'Estrada', '3'],
            3 => ['tilda007', 'til123', 'Tilda', 'Padilla', '4']
        ];

        // dd($arrs);
        foreach($arrs as $arr){
            // dd($arr);
            
            $data = new Users;
            $data->username = $arr[0];
            $data->password = Hash::make($arr[1]);
            $data->first_name = $arr[2];
            $data->last_name = $arr[3];
            $data->position_id = $arr[4];
            $data->save();
        }
        // dd($arr);
    }

    public function index(){
        return view('login');
    }

    public function login(Request $request){
        // dd($request->except('_token'));

        $this->validate($request, [
            'username'=> 'required|max:255',
            'password'=> 'required|max:255'
        ]);

        $username = $request->input('username');
        $password = $request->input('password');

        $user = Users::where('username', $username)->first();
        // dd($user);

        if($user == null){
            return "ไม่พบ User";
        }

        if(Hash::check($password, $user->password)){
            // return "success";
            session([
                'user_id'=> $user->id,
                'username'=> $username,
            ]);
          
            $user->last_login_at = date("Y-m-d H:i:s");
            $user->save();
            return redirect('show_user');
        }else{
            return "รหัสผ่านไม่ถูกต้อง";
        }
    }

    public function show_user(){
        // $user_id = session('user_id');
        $username = session('username');
        // dd($username);
        
        if(!$username){
            return "ท่านยังไม่เข้าสู่ระบบ โปรดเข้าสู่ระบบก่อน";
        }
        $user = Users::where('username', $username)->first();
        $users_data = Users::get();
        
        return view('show_user')->with([
            'user' => $user,
            'users_data' => $users_data
        ]);
    }

    public function delete_user($id, Request $request){
        // dd($request->except('token'));
        // dd($id);

        $user = Users::where('id', $id)->first();
        if(!$user){
            return "ไม่พบ user";
        }

        $user->delete();
        return redirect('show_user');
    }
}
