<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $id;
    protected $position;

    public $table = 'Position';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

}
