<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $id;
    protected $username;
    protected $password;
    protected $first_name;
    protected $last_name;
    protected $position_id;
    protected $last_login_at;

    public $table = 'Users';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

    public function position(){
        return $this->hasOne('App\position', 'id', 'position_id');
    }
}
